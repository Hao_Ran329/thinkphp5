<?php

namespace app\admin\controller;
use think\Session;

class Index extends Base
{
    public function index(){
       return $this->fetch();
    }
    public function welcome()
    {
        $username = Session::get('username');
        $this->assign('username', $username);
        return $this->fetch();
    }
}