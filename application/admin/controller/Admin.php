<?php
namespace app\admin\controller;
use think\Controller;

class Admin extends Controller {
    public function admin(){
        $student = [
            [
                'name' =>'小明',
                'age' =>20,
                'sex' => '男',
                'address'=>'山东省泰安市'
            ],
            [
                'name' => '小红',
                'age' => 21,
                'sex' => '女',
                'address'=>'山东省泰安市'
            ],
            [
                'name' => '小亮',
                'age' => 18,
                'sex' => '男',
                'address'=>'山东省泰安市'
            ],
        ];
        $this->assign('student',$student);
        return $this->fetch();
    }
}