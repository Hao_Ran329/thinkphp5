<?php

namespace app\index\model;

use think\Model;

class Student extends Model
{
    protected $table = "tp_student";
    public function Classroom(){
        return $this->hasOne('Classroom','id','class_id');
    }
}