<?php
namespace app\index\model;

use think\Model;

class Classroom extends Model
{
    protected $table = 'tp_class';

    public function Major()
    {
        return $this->hasOne('Major', 'id', 'major_id');
    }
}