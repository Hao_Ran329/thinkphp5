<?php
namespace app\index\model;

use think\Model;

class Major extends Model{
    protected $table = 'tp_major';
    public function School()
    {
        return $this->hasOne('School','id','school_id');
    }
}