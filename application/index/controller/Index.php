<?php
namespace app\index\controller;

use think\Controller;
use think\Session;

class Index extends Controller
{
    public function index(){
        return $this->fetch();
    }
    public function welcome()
    {
        $username = Session::get('username');
        $this->assign('username', $username);
        return $this->fetch();
    }
}
